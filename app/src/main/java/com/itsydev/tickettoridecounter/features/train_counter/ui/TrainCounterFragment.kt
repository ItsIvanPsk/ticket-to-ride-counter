package com.itsydev.tickettoridecounter.features.train_counter.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.material.checkbox.MaterialCheckBox
import com.itsydev.tickettoridecounter.R
import com.itsydev.tickettoridecounter.core.BaseFragment
import com.itsydev.tickettoridecounter.databinding.FragmentTrainCounterBinding

class TrainCounterFragment : BaseFragment<FragmentTrainCounterBinding>(),
    RouteSelectorDialogListener {

    private val viewmodel: TrainCounterViewModel by viewModels()
    private lateinit var routeDialog: RouteSelectorDialog
    override fun createViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentTrainCounterBinding {
        return FragmentTrainCounterBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        routeDialog = RouteSelectorDialog(requireContext(), this@TrainCounterFragment)
        setupTrainButtonListeners()
        setupCheckBoxListeners()
        setupScoreObservers()
    }

    private fun setupCheckBoxListeners() = with(binding) {
        tcPlayerMaxRoutes.setOnClickListener {
            Log.d("5cos", it.toString())
            viewmodel.updateMaxRoutes((it as MaterialCheckBox).isChecked)
            viewmodel.updateTotalScore()
        }
        tcPlayerMaxTrainRoute.setOnClickListener {
            viewmodel.updateMaxTrainLenght((it as MaterialCheckBox).isChecked)
            viewmodel.updateTotalScore()
        }
    }

    private fun setupScoreObservers() = with(viewmodel) {
        getTrainScore().observe(viewLifecycleOwner) {
            binding.tcPlayerTrainScoreValue.text = it.toString()
            checkButtonsAvailability(it)
            updateTotalScore()
        }
        getRouteScore().observe(viewLifecycleOwner) {
            binding.tcPlayerRouteScore.text = getString(R.string.routes_points, it.toString())
            updateTotalScore()
        }
        getTotalScore().observe(viewLifecycleOwner) {
            binding.tcPlayerTotalScoreValue.text = it.toString()
        }
    }

    private fun setupTrainButtonListeners() = with(binding) {
        listOf(
            tcTrainOneImageAdd to 1,
            tcTrainOneImageSubstract to -1,
            tcTrainTwoImageAdd to 2,
            tcTrainTwoImageSubstract to -2,
            tcTrainThreeImageAdd to 4,
            tcTrainThreeImageSubstract to -4,
            tcTrainFourImageAdd to 7,
            tcTrainFourImageSubstract to -7,
            tcTrainFiveImageAdd to 10,
            tcTrainFiveImageSubstract to -10,
            tcTrainSixImageAdd to 15,
            tcTrainSixImageSubstract to -15
        ).forEach { (btn, value) ->
            btn.setOnClickListener {
                viewmodel.updateTrainScore(value)
            }
        }
        tcPlayerRouteScore.setOnClickListener {
            routeDialog.show()
        }
    }

    private fun checkButtonsAvailability(trainScore: Int) = with(binding) {
        listOf(
            tcTrainOneImageSubstract to -1,
            tcTrainTwoImageSubstract to -2,
            tcTrainThreeImageSubstract to -4,
            tcTrainFourImageSubstract to -7,
            tcTrainFiveImageSubstract to -10,
            tcTrainSixImageSubstract to -15
        ).forEach { (button, value) ->
            button.isEnabled = trainScore >= -value
        }
    }

    override fun updateScore(score: Int) {
        viewmodel.updateRouteScore(score)
        routeDialog.dismiss()
    }

}
