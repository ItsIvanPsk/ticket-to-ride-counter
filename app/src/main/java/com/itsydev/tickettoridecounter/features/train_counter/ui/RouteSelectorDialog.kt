package com.itsydev.tickettoridecounter.features.train_counter.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import com.itsydev.tickettoridecounter.databinding.RouteSelectorDialogBinding

class RouteSelectorDialog(
    private val context: Context,
    private val listener: RouteSelectorDialogListener
) : Dialog(context) {

    private lateinit var binding: RouteSelectorDialogBinding

    private var cen = 0
    private var dec = 0
    private var uds = 0

    private var maxNumber = 9
    private var minNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = RouteSelectorDialogBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)
        setupListeners()
    }

    private fun setupListeners() = with(binding) {
        routeAddCen.setOnClickListener {
            if (cen != maxNumber) {
                cen++
            }
            updateScores()
        }

        routeLessCen.setOnClickListener {
            if (cen > minNumber) {
                cen--
            }
            updateScores()
        }

        routeAddDec.setOnClickListener {
            if (dec != maxNumber) {
                dec++
            }
            updateScores()
        }

        routeLessDec.setOnClickListener {
            if (dec > minNumber) {
                dec--
            }
            updateScores()
        }

        routeAddUds.setOnClickListener {
            if (uds != maxNumber) {
                uds++
            }
            updateScores()
        }

        routeLessUds.setOnClickListener {
            if (uds > minNumber) {
                uds--
            }
            updateScores()
        }

        routeSaveScore.setOnClickListener {
            listener.updateScore((cen * 100) + (dec * 10) + uds)
        }
    }

    private fun updateScores() {
        binding.routeScoreCen.text = cen.toString()
        binding.routeScoreDec.text = dec.toString()
        binding.routeScoreUds.text = uds.toString()
    }
}

interface RouteSelectorDialogListener {
    fun updateScore(score: Int)
}
