package com.itsydev.tickettoridecounter.features.train_counter.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TrainCounterViewModel : ViewModel() {

    private var trainScore = MutableLiveData(0)
    private var routeScore = MutableLiveData(0)
    private var totalScore = MutableLiveData(0)

    private var hasMaxRoutes = MutableLiveData(false);
    private var hasMaxTrainLength = MutableLiveData(false);

    fun getTrainScore() = trainScore
    fun getRouteScore() = routeScore
    fun getTotalScore() = totalScore

    fun updateTrainScore(addScore: Int) {
        trainScore.value?.let {
            trainScore.value = it + addScore
        }
    }

    fun updateRouteScore(addScore: Int) {
        routeScore.value = addScore
        updateTotalScore()
    }

    fun updateTotalScore() {
        trainScore.value?.let { train ->
            routeScore.value?.let { route ->
                var score = train + route
                if (score > 999) {
                    score = 999
                }
                Log.d("5cos", score.toString())
                if (hasMaxRoutes.value == true) {
                    score += 15
                }
                if (hasMaxTrainLength.value == true) {
                    score += 10
                }
                totalScore.value = score
                Log.d("5cos", totalScore.value.toString())
            }
        }
    }

    fun updateMaxRoutes(isSelected: Boolean) {
        hasMaxRoutes.value = isSelected
    }

    fun updateMaxTrainLenght(isSelected: Boolean) {
        hasMaxTrainLength.value = isSelected
    }

}